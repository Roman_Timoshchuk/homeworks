"use strict";

function checkAge(age) {
    return ((isNaN(age) || age < 1 || age > 120))
}

function checkName(name) {
    return (name === null || name === "" || isNaN(name) === false )
}


let name = prompt("Enter your name.");
while (checkName(name))
    name = prompt("Please enter you name again");

let age = +prompt("Enter your age");
while (checkAge(age)) {
    age = prompt("Please enter you age again");
}
if (age < 18) {
    alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
    let result = confirm("Are you sure you want to continue?");
    if (result === true) {
        alert("Welcome, " + name)
    } else {
        alert("You are not allowed to visit this website");
    }
} else alert("Welcome, " + name);
